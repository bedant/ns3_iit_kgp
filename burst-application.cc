/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright 2007 University of Washington
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author:  Tom Henderson (tomhend@u.washington.edu)
 */
#include "ns3/address.h"
#include "ns3/address-utils.h"
#include "ns3/log.h"
#include "ns3/inet-socket-address.h"
#include "ns3/inet6-socket-address.h"
#include "ns3/node.h"
#include "ns3/socket.h"
#include "ns3/udp-socket.h"
#include "ns3/simulator.h"
#include "ns3/socket-factory.h"
#include "ns3/packet.h"
#include "ns3/trace-source-accessor.h"
#include "ns3/udp-socket-factory.h"
#include "burst-application.h"

namespace ns3 {

NS_LOG_COMPONENT_DEFINE ("BurstApp");

NS_OBJECT_ENSURE_REGISTERED (BurstApp);

TypeId 
BurstApp::GetTypeId (void)
{
  static TypeId tid = TypeId ("ns3::BurstApp")
    .SetParent<Application> ()
    .SetGroupName("Applications")
    .AddConstructor<BurstApp> ()
    .AddAttribute ("Remote", "The address of the destination",
                   AddressValue (),
                   MakeAddressAccessor (&BurstApp::m_peer),
                   MakeAddressChecker ())
    .AddAttribute ("Protocol",
                   "The type id of the protocol to use for the rx socket.",
                   TypeIdValue (UdpSocketFactory::GetTypeId ()),
                   MakeTypeIdAccessor (&BurstApp::m_tid),
                   MakeTypeIdChecker ())
  ;
  return tid;
}

BurstApp::BurstApp ()
{
  NS_LOG_FUNCTION (this);
  m_socket = 0;

  // m_totalRx = 0;
}

BurstApp::~BurstApp()
{
  NS_LOG_FUNCTION (this);
}

// uint64_t BurstApp::GetTotalRx () const
// {
//   NS_LOG_FUNCTION (this);
//   return m_totalRx;
// }

// Ptr<Socket>
// BurstApp::GetListeningSocket (void) const
// {
//   NS_LOG_FUNCTION (this);
//   return m_socket;
// }

// std::list<Ptr<Socket> >
// BurstApp::GetAcceptedSockets (void) const
// {
//   NS_LOG_FUNCTION (this);
//   return m_socketList;
// }

// void BurstApp::DoDispose (void)
// {
//   NS_LOG_FUNCTION (this);
//   m_socket = 0;
//   m_socketList.clear ();

//   // chain up
//   Application::DoDispose ();
// }


// Application Methods
void BurstApp::StartApplication ()    // Called at time specified by Start
{
  if (!m_socket)
  {
    m_socket = Socket::CreateSocket (GetNode (), m_tid);
    m_packetSize = 104;
    m_nPackets = 10;
    m_iters = 4;
    m_itersDone = 0;
    m_running = true;
    m_packetsSent = 0;
    m_socket->Bind ();
    m_socket->Connect (m_peer);
    m_socket->SetSendCallback (MakeCallback (&BurstApp::SendManyPackets,this));
    SendManyPackets (m_socket, m_socket->GetTxAvailable());
  }
}

void BurstApp::StopApplication ()     // Called at time specified by Stop
{
  m_running = false;

  if (m_sendEvent.IsRunning ())
  {
    Simulator::Cancel (m_sendEvent);
  }

  if (m_socket)
  {
    m_socket->Close ();
  }
}

void 
BurstApp::SendManyPackets (Ptr<Socket> localSocket, uint32_t txSpace)
{
  // std::cout<<"smp\n";
  Ptr<Packet> packet = Create<Packet> (m_packetSize);
  //send many packets at a time
  if(m_itersDone < m_iters )
  {
    while (m_packetsSent < m_nPackets)
      {
          if(localSocket->GetTxAvailable () < m_packetSize)
          {
            std::cout<<"returned\n";
            return;
          }
          int AmtSent = localSocket->Send (packet);//sendavail //
          std::cout<<"AmtSent="<<AmtSent<<"\n";
          m_packetsSent++;
      }
      m_itersDone++;
  }
  m_packetsSent = 0;
  CallScheduler();
}

void 
BurstApp::CallScheduler (void)
{
  if (m_running)
    {
      Time tNext (Seconds (5));
      m_sendEvent = Simulator::Schedule (tNext, &BurstApp::SendManyPackets, this, m_socket, m_socket->GetTxAvailable());
    }
}


} // Namespace ns3
