#ifndef LARGE_TRANSFER_H
#define LARGE_TRANSFER_H

#include "ns3/application.h"
#include "ns3/event-id.h"
#include "ns3/ptr.h"
#include "ns3/traced-callback.h"
#include "ns3/address.h"
#include "ns3/uinteger.h"
namespace ns3 {

class Address;
class Socket;
class Packet;

class LargeTransferApp : public Application
{
public:
  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId (void);
  LargeTransferApp ();
  virtual ~LargeTransferApp();

  void Setup (Ptr<Socket> socket, Address address, uint32_t packetSize, uint32_t nPackets, uint32_t iters);

private:
  virtual void StartApplication (void);
  virtual void StopApplication (void);

  void Setup(uint32_t totalTxBytes, uint32_t writeSize);
  void StartFlow (void);
  void WriteUntilBufferFull (Ptr<Socket> localSocket, uint32_t txSpace);

  TypeId          m_tid;
  Ptr<Socket>     m_socket;
  Address         m_peer;
  uint32_t        m_totalTxBytes;
  uint32_t        m_currentTxBytes;
// Perform series of 1040 byte writes (this is a multiple of 26 since
// we want to detect data splicing in the output stream)
  uint32_t        m_writeSize;
  uint8_t         m_data[1040];
};

} // namespace ns3

#endif /* PACKET_SINK_H */


