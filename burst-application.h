#ifndef BURST_APPLICATION_H
#define BURST_APPLICATION_H

#include "ns3/application.h"
#include "ns3/event-id.h"
#include "ns3/ptr.h"
#include "ns3/traced-callback.h"
#include "ns3/address.h"

namespace ns3 {

class Address;
class Socket;
class Packet;

class BurstApp : public Application
{
public:
  /**
   * \brief Get the type ID.
   * \return the object TypeId
   */
  static TypeId GetTypeId (void);
  BurstApp ();
  virtual ~BurstApp();

  void Setup (Ptr<Socket> socket, Address address, uint32_t packetSize, uint32_t nPackets, uint32_t iters);

private:
  virtual void StartApplication (void);
  virtual void StopApplication (void);

  void CallScheduler (void);
  void SendManyPackets (Ptr<Socket> localSocket, uint32_t txSpace);

  Ptr<Socket>     m_socket;
  Address         m_peer;
  uint32_t        m_packetSize;
  uint32_t        m_nPackets;
  EventId         m_sendEvent;
  bool            m_running;
  uint32_t        m_packetsSent;
  uint32_t		  m_iters;
  uint32_t		  m_itersDone;
  TypeId          m_tid; 

};

} // namespace ns3

#endif /* PACKET_SINK_H */


