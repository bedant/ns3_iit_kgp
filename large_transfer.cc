
#include "ns3/address.h"
#include "ns3/address-utils.h"
#include "ns3/log.h"
#include "ns3/inet-socket-address.h"
#include "ns3/inet6-socket-address.h"
#include "ns3/node.h"
#include "ns3/socket.h"
#include "ns3/udp-socket.h"
#include "ns3/simulator.h"
#include "ns3/socket-factory.h"
#include "ns3/packet.h"
#include "ns3/trace-source-accessor.h"
#include "ns3/udp-socket-factory.h"
#include "large_transfer.h"

namespace ns3 {

NS_LOG_COMPONENT_DEFINE ("LargeTransferApp");

NS_OBJECT_ENSURE_REGISTERED (LargeTransferApp);

TypeId 
LargeTransferApp::GetTypeId (void)
{
  static TypeId tid = TypeId ("ns3::LargeTransferApp")
    .SetParent<Application> ()
    .SetGroupName("Applications")
    .AddConstructor<LargeTransferApp> ()
    .AddAttribute ("Remote", "The address of the destination",
                   AddressValue (),
                   MakeAddressAccessor (&LargeTransferApp::m_peer),
                   MakeAddressChecker ())
    .AddAttribute ("Protocol",
                   "The type id of the protocol to use for the rx socket.",
                   TypeIdValue (UdpSocketFactory::GetTypeId ()),
                   MakeTypeIdAccessor (&LargeTransferApp::m_tid),
                   MakeTypeIdChecker ())
    .AddAttribute ("totalTxBytes",
                   "The total number of bytes to send. "
                   "Once these bytes are sent, "
                   "no data  is sent again." ,
                   UintegerValue (0),
                   MakeUintegerAccessor (&LargeTransferApp::m_totalTxBytes),
                   MakeUintegerChecker<uint64_t> ())
    .AddAttribute ("writeSize",
                   "The size of buffer" ,
                   UintegerValue (0),
                   MakeUintegerAccessor (&LargeTransferApp::m_writeSize),
                   MakeUintegerChecker<uint64_t> ())
  ;
  return tid;
}

LargeTransferApp::LargeTransferApp ()
  : m_socket (0),
	  m_peer ()
{
}

LargeTransferApp::~LargeTransferApp()
{
  m_socket = 0;
}

void 
LargeTransferApp::Setup( uint32_t totalTxBytes, uint32_t writeSize)
{
	// m_socket = socket;
	// m_peer = address;
	m_totalTxBytes = totalTxBytes;
	m_writeSize = writeSize;
	for(uint32_t i = 0; i < writeSize; ++i)
    {
      char m = toascii (97 + i % 26);
      m_data[i] = m;
    }
}

void
LargeTransferApp::StartApplication (void)
{
  std::cout<<"starting Application\n";
  if (!m_socket)
  {
    m_socket = Socket::CreateSocket (GetNode (), m_tid);
    m_currentTxBytes = 0;
    m_socket->Bind ();
    m_socket->Connect (m_peer);
    std::cout<<"sf\n";
    StartFlow ();
  }
}


void 
LargeTransferApp::StopApplication (void)
{
 
 //  if (m_sendEvent.IsRunning ())
	// {
	//   Simulator::Cancel (m_sendEvent);
	// }

  if (m_socket)
	{
	  m_socket->Close ();
	}
}

void LargeTransferApp::StartFlow ()
{
  // localSocket = m_socket;
  NS_LOG_LOGIC ("Starting flow at time " <<  Simulator::Now ().GetSeconds ());
  // localSocket->Connect (InetSocketAddress (servAddress, servPort)); //connect

  // tell the tcp implementation to call WriteUntilBufferFull again
  // if we blocked and new tx buffer space becomes available
  m_socket->SetSendCallback (MakeCallback (&LargeTransferApp::WriteUntilBufferFull,this));
  WriteUntilBufferFull (m_socket, m_socket->GetTxAvailable ());
}

void LargeTransferApp::WriteUntilBufferFull (Ptr<Socket> localSocket, uint32_t txSpace)
{
  while (m_currentTxBytes < m_totalTxBytes && localSocket->GetTxAvailable () > 0) 
    {
      uint32_t left = m_totalTxBytes - m_currentTxBytes;
      uint32_t dataOffset = m_currentTxBytes % m_writeSize;
      uint32_t toWrite = m_writeSize - dataOffset;
      toWrite = std::min (toWrite, left);
      toWrite = std::min (toWrite, localSocket->GetTxAvailable ());
      int amountSent = localSocket->Send (&m_data[dataOffset], toWrite, 0);
      if(amountSent < 0)
        {
          // we will be called again when new tx space becomes available.
        	// std::cout<<amountSent<<"=--------------returning----------- avail="<<localSocket->GetTxAvailable ()<<" ;"<<"tr"<<toWrite<<";;; "<<m_currentTxBytes<<"\n";
          return;
        }
      m_currentTxBytes += amountSent;
      // std::cout<<"curr sent="<<m_currentTxBytes<<" ;; avail="<<localSocket->GetTxAvailable ()<<std::endl;
    }
  localSocket->Close ();
}


}