/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */

#include "ns3/core-module.h"
#include "ns3/largeandburst-helper.h"
// #include "uinteger.h"

using namespace ns3;


int 
main (int argc, char *argv[])
{
  bool verbose = true;

  CommandLine cmd;
  cmd.AddValue ("verbose", "Tell application to log if true", verbose);

  cmd.Parse (argc,argv);


  LogComponentEnable("PacketSink",LOG_LEVEL_INFO);

  NodeContainer serverNode;
  NodeContainer clientNodes;
  uint32_t N = 6; //number of nodes in the star
  serverNode.Create (1);
  clientNodes.Create (N-1);
  NodeContainer allNodes = NodeContainer (serverNode, clientNodes);
  
  InternetStackHelper internet;
  internet.Install (allNodes);

  std::vector<NodeContainer> nodeAdjacencyList (N-1);
   for(uint32_t i=0; i<nodeAdjacencyList.size (); ++i)
   {
     nodeAdjacencyList[i] = NodeContainer (serverNode, clientNodes.Get (i));
   }
 
   // We create the channels first without any IP addressing information
   // NS_LOG_INFO ("Create channels.");
   PointToPointHelper p2p;
   p2p.SetDeviceAttribute ("DataRate", StringValue ("5Mbps"));
   p2p.SetChannelAttribute ("Delay", StringValue ("2ms"));
   std::vector<NetDeviceContainer> deviceAdjacencyList (N-1);
   for(uint32_t i=0; i<deviceAdjacencyList.size (); ++i)
   {
     deviceAdjacencyList[i] = p2p.Install (nodeAdjacencyList[i]);
   }

   Ipv4AddressHelper ipv4;
   std::vector<Ipv4InterfaceContainer> interfaceAdjacencyList (N-1);
   for(uint32_t i=0; i<interfaceAdjacencyList.size (); ++i)
     {
       std::ostringstream subnet;
       subnet<<"10.1."<<i+1<<".0";
       ipv4.SetBase (subnet.str ().c_str (), "255.255.255.0");
       interfaceAdjacencyList[i] = ipv4.Assign (deviceAdjacencyList[i]);
     }
 
  //Turn on global static routing
   Ipv4GlobalRoutingHelper::PopulateRoutingTables ();

   uint16_t port = 50000;
   Address sinkLocalAddress (InetSocketAddress (Ipv4Address::GetAny (), port));
   PacketSinkHelper sinkHelper ("ns3::TcpSocketFactory", sinkLocalAddress);
   ApplicationContainer sinkApp = sinkHelper.Install (serverNode);
  sinkApp.Start (Seconds (0.));
  sinkApp.Stop (Seconds (60.));

  
  BurstAppHelper burstAppHelper ("ns3::TcpSocketFactory", Address());
  LargeTransferHelper largeTransferHelper ("ns3::TcpSocketFactory", Address());
  largeTransferHelper.SetAttribute("writeSize",UintegerValue (20));
  largeTransferHelper.SetAttribute("totalTxBytes",UintegerValue(20000));

  ApplicationContainer clientApps;
   for(uint32_t i=0; i<clientNodes.GetN (); ++i)
     {
       AddressValue remoteAddress
         (InetSocketAddress (interfaceAdjacencyList[i].GetAddress (0), port));
       burstAppHelper.SetAttribute ("Remote", remoteAddress);
       largeTransferHelper.SetAttribute ("Remote", remoteAddress);
       clientApps.Add (burstAppHelper.Install (clientNodes.Get (i)));
       clientApps.Add (largeTransferHelper.Install (clientNodes.Get (i)));
     }
   clientApps.Start (Seconds (1.0));
   clientApps.Stop (Seconds (60.0));

  Simulator::Stop (Seconds (60));
  Simulator::Run ();
  Simulator::Destroy ();

  return 0;
}


